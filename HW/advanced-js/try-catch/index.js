const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const container = document.getElementById('root');
const list = document.createElement('ul');
container.append(list);

function renderData ({author, name, price}) {
    if (author && name && price) {
        list.insertAdjacentHTML('afterbegin', `<li>${author}, ${name}, ${price}</li>`);
    } else {
        if (!author) {
            throw new Error(`Missing property author in "${name}" book`);
        } else if (!name) {
            throw new Error(`Missing property name in "${name}" book`);
        } else if (!price) {
            throw new Error(`Missing property price in "${name}" book`);
        }
    }
}

function dataChecker(data) {
    data.forEach(({author, name, price}) => {
        try {
            renderData({author, name, price});
        } catch (e) {
            console.error(e.message);
        }
    });
}

dataChecker(books)


