1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.


   Try...catch конструкцію доречно використовувати в ситуаціях коли потенційно ми очікуємо помилку.
   Тобто наприклад try catch можна використати в промісах де ми обробляємо запити на сервер, таким чином якщо від серверу ми отримали помилку то надалі ми можемо її якось обробити, щоб наш додаток не впав повністю.
   