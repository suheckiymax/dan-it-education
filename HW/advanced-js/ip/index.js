class LocationByIP {
    constructor() {
        this.elements = {
            btn: document.createElement('button'),
            p: document.createElement('p')
        }
    }

    async getData() {
        const ip = await fetch('https://api.ipify.org/?format=json');
        const clientIP = await ip.json();
        const clientAddress = await fetch(`http://ip-api.com/json/${clientIP.ip}?fields=continent,country,regionName,city,district`);
        return await clientAddress.json()
    }

    async handleSubmit() {
        const {btn, p} = this.elements;
        const {continent, country, regionName, city, district} = await this.getData();
        p.append(`Континент - ${continent}; Страна - ${country}; Регион - ${regionName}; Город - ${city}; Район - ${district}`);
        btn.after(p);
    }

    render() {
        const {btn, p} = this.elements;

        p.textContent = 'Местоположение: ';
        btn.textContent = 'Вычислить по IP';
        btn.classList.add('btn');
        btn.addEventListener('click', () => this.handleSubmit());

        document.body.append(btn);
    }
}

const locationIp = new LocationByIP();
locationIp.render()
