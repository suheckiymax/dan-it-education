const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = {...employee, age: 25, salary: 5000}

console.log('Task 6', newEmployee)