class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }

    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        super._salary = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const maksym = new Programmer('Max', 20, 3000, ['ua', 'en']);
const yaroslav = new Programmer('Yaroslav', 38, 5000, ['ua']);
console.log(maksym, yaroslav);
console.log(`Maksym salary: ${maksym.salary};\nYaroslav salary: ${yaroslav.salary};`);