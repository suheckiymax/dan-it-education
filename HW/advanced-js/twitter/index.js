class API {
    constructor() {
        this.BASE_URL = 'https://ajax.test-danit.com/api/json';
    }

    async getUsers() {
        const response = await fetch(`${this.BASE_URL}/users`);
        return response.json()
    }

    async getPosts() {
        const response = await fetch(`${this.BASE_URL}/posts`);
        return response.json()
    }

    async deletePost(postId) {
        const response = await fetch(`${this.BASE_URL}/posts/${postId}`, {
            method: 'DELETE'
        })

        return response.status
    }
}

class Card {
    async deletePost(id) {
        const api = new API();
        const status = await api.deletePost(id);

        if (status === 200) {
            const currentPost = document.querySelector(`[data-post="${id}"]`);
            currentPost.remove();
        }
    }
    render(data, container) {
        const {posts, users} = data;

        posts.map(({title, body, id, userId}) => {
            const {name, email} = users.find(({id}) => id === userId);
            container.insertAdjacentHTML('afterbegin', `
                <div class="post" data-post="${id}">
                    <h4 class="post__title">${title}</h4>
                    <p class="post__text">${body}</p>
                    <p class="post__author">${name}</p>
                    <p class="post__author-email">${email}</p>
                    <button class="post__btn">Delete post</button>
                </div>
            `)
        });

    }
}

document.addEventListener('DOMContentLoaded', async () => {
    const container = document.querySelector('.posts');
    const api = new API();
    const card = new Card();
    const posts = await api.getPosts();
    const users = await api.getUsers();
    await card.render({posts, users}, container);

    const deletePostBtns = document.querySelectorAll('.post__btn');
    deletePostBtns.forEach(btn => {
        btn.addEventListener('click', event => {
            const postId = event.target.parentElement.dataset.post;
            card.deletePost(postId)
        })
    })
});