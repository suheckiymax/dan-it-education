const url = 'https://ajax.test-danit.com/api/swapi/films'

fetch(url)
    .then(res => res.json())
    .then(data => {
        const container = document.getElementById('root')

        data.map(item => {
            container.insertAdjacentHTML('afterbegin', `
                <section class="film">
                    <span class="film-episode">Episode - ${item.episodeId}</span>
                    <h1 class="film-title">${item.name}</h1>
                    <p class="film-characters"></p>
                    <p class="film-desc">${item.openingCrawl}</p>
                </section>
            `)

            const requests = item.characters.map(url => fetch(url))
            const charactersBox = document.querySelector('.film-characters')

            Promise.all(requests)
                .then(responses => Promise.all(responses.map(r => r.json())))
                .then(characters => characters.forEach(character => {
                    charactersBox.append(`${character.name}, `)
                }))
        })
    })
