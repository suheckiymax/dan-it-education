import types from "./types";

const getDataReducer = (store = [], action) => {
    switch (action.type) {
        case types.GET_DATA:
            return action.payload
        case types.SET_PRODUCT_CART:
            return store.map(product => {
                return product.code === action.payload.code ? {
                    ...action.payload,
                    inCart: true
                } : product
            })
        case types.SET_PRODUCT_FAVORITE:
            return store.map(product => {
                return product.code === action.payload.code ? {
                    ...action.payload,
                    isFavorite: !action.payload.isFavorite
                } : product
            })
        case types.REMOVE_PRODUCT_CART:
            return store.map(product => {
                return {
                    ...product,
                    inCart: false
                }
            })
        default:
            return store
    }
}

export default getDataReducer