import {types} from "./index";

const activeModalReducer = (state = {}, action) => {
    switch (action.type) {
        case types.SET_MODAL:
            return action.payload
        default:
            return state
    }
}

export default activeModalReducer
