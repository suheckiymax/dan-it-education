import React from "react";
import './Cart.scss'
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import ProductList from "../../components/ProductList/ProductList";
import {useSelector} from "react-redux";
import {selectData} from "../../redux/getData/selectors";
import {useProductHook} from "../../utils/customHooks/useProductHook";

const Cart = () => {
    const listData = useSelector(selectData);
    const {activeProduct, favoriteProduct} = useProductHook();

    const products = listData.filter(product => product.inCart);
    if (!products.length) return <section className='cart cart__default-text'>No products selected</section>

    return (
        <section className='cart container'>
            <OrderSummary/>
            <ProductList list={products}
                         favoriteProduct={favoriteProduct}
                         handleClick={activeProduct}
                         status='delete'
                         btnClasses='cart__control-delete'
                         btnText='Delete from cart'
                         isCart={true}
            />
        </section>
    )
}

export default Cart