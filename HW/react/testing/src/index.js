import React, {useState} from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import CardsModeContext from "./context/cardsViewContext";
import './assets/styles/reset.scss';

const Main = () => {
    const [cardsView, setCardsView] = useState('grid');

    return (
        <React.StrictMode>
            <BrowserRouter>
                <Provider store={store}>
                    <CardsModeContext.Provider value={{cardsView, setCardsView}}>
                        <App/>
                    </CardsModeContext.Provider>
                </Provider>
            </BrowserRouter>
        </React.StrictMode>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Main/>);
