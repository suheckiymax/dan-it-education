import {types} from "./index";

const activeProductReducer = (state = {}, action) => {
    switch (action.type) {
        case types.SET_PRODUCT:
            return action.payload
        default:
            return state
    }
}

export default activeProductReducer
