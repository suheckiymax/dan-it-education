import React from "react";
import {Routes, Route} from 'react-router-dom';
import {RoutesConfig} from './Routes';

const AppRouter = () => {
    return (
        <Routes>
            {RoutesConfig.map(route => (
                <Route key={route.path}
                       path={route.path === '' ? '' : route.path}
                       index={route.path === '/'}
                       element={route.component}
                />
            ))}
        </Routes>
    )
}

export default AppRouter