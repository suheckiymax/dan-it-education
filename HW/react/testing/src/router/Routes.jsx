import React from "react";
import Home from "../containers/Home/Home";
import Cart from "../containers/Cart/Cart";
import Favorites from "../containers/Favorites/Favorites";
import Page404 from "../containers/NotFound/Page404";

export const RoutesConfig = [
    {
        path: '',
        component: <Home/>
    },
    {
        path: '/cart',
        component: <Cart/>
    },
    {
        path: '/favorites',
        component: <Favorites/>
    },
    {
        path: '*',
        component: <Page404/>
    }
]