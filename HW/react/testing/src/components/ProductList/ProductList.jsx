import React from 'react';
import PropTypes, {object} from 'prop-types'
import Product from "../Product/Product";
import './ProductList.scss'

const ProductList = ({list, favoriteProduct, handleClick, isCart, option}) => {
    const productsList = list.map(product =>
        <Product key={product.code}
                 code={product.code}
                 name={product.name}
                 price={product.price}
                 picture={product.picture}
                 color={product.color}
                 inCart={product.inCart}
                 isFavorite={product.isFavorite}
                 product={product}
                 favoriteProduct={favoriteProduct}
                 handleClick={handleClick}
                 isCart={isCart}
                 option={option}
        />
    )

    switch (option) {
        case 'list':
            return (
                <table className='products-list'>
                    <tbody>
                        {productsList}
                    </tbody>
                </table>
            )
        case 'grid':
            return (
                <div className='products-grid'>
                    {productsList}
                </div>
            )
    }
}

export default ProductList

ProductList.propTypes = {
    list: PropTypes.arrayOf(object).isRequired,
    favoriteProduct: PropTypes.func,
    handleClick: PropTypes.func.isRequired,
    isCart: PropTypes.bool
}

ProductList.defaultProps = {
    option: 'grid'
}