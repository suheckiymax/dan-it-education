import * as yup from 'yup';

const orderSummarySchema = yup.object().shape({
    name: yup.string()
        .min(4, 'Less then 4 symbols')
        .required('This field is required'),
    lastName: yup.string()
        .min(5, 'Less then 5 symbols')
        .required('This field is required'),
    age: yup.number()
        .lessThan(100, 'Are you sure this is your age?')
        .positive('Not born yet?')
        .required('This field is required'),
    address: yup.string()
        .min(8, 'Less then 8 symbols')
        .required('This field is required'),
    phone: yup.number()
        .min(10, 'Less then 10 symbols')
        .required('This field is required')
})

export default orderSummarySchema