import React from 'react';
import PropTypes from 'prop-types'
import './Product.scss'
import {favorite} from "../../utils/index";
import Button from "../Button/Button";

const Product = (props) => {
    const {
        product,
        picture,
        name,
        color,
        price,
        handleClick,
        favoriteProduct,
        code,
        isCart,
        option
    } = props;

    switch (option) {
        case 'list':
            return (
                <tr>
                    <td className='product-list__cell'>
                        <img src={picture} alt={name} className='product-list__img'/>
                    </td>
                    <td className='product-list__cell'>
                        <p className='product-list__about-title'>{name}</p>
                        <br/>
                        <p className='product-list__code'>Code: {code}</p>
                    </td>
                    <td className='product-list__cell'>
                        ({color})
                    </td>
                    <td className='product-list__cell'>
                        <p className='product-list__price'>{price}</p>
                    </td>
                    <td className='product-list__cell'>
                        <button onClick={() => favoriteProduct(product)} className='product-list__favorite-btn'>
                            {product.isFavorite ? favorite('1', 'product-list__favorite') : favorite('0', 'product-list__favorite')}
                        </button>
                    </td>
                </tr>
            )
        case 'grid':
            return (
                <div className='product-grid'>
                    <div className='product-grid__header'>
                        <p className='product-grid__header-code'>Code: {code}</p>
                        <button className='product-grid__favorite-btn' onClick={() => favoriteProduct(product)}>
                            {product.isFavorite ? favorite('1', 'product-grid__favorite') : favorite('0', 'product-grid__favorite')}
                        </button>
                    </div>
                    <div className='product-grid__about'>
                        <img src={picture} alt={name} className='product-grid__header-img'/>
                        <p className='product-grid__about-title'>{name} <br/> ({color})</p>
                    </div>
                    <div className='product-grid__control'>
                        <p className='product-grid__control-price'>{price}</p>
                        {!isCart && <Button classes="button button--smaller button--primary"
                                            text='Add to cart'
                                            handleClick={() => handleClick('add', product)}/>}
                    </div>
                </div>
            )
    }
}

export default Product;

Product.propTypes = {
    picture: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired,
    favoriteProduct: PropTypes.func.isRequired,
    code: PropTypes.string.isRequired
}

Product.defaultProps = {
    color: ''
}