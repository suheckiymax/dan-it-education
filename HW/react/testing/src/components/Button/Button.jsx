import React from "react";
import PropTypes from 'prop-types'
import './Button.scss'

const Button = ({classes, handleClick, text}) => {
    return <button data-testid='btn' className={classes} onClick={handleClick}>{text}</button>
}

export default Button

Button.propTypes = {
    classes: PropTypes.string,
    text: PropTypes.string.isRequired
}

Button.defaultProps = {
    classes: 'default-Button',
}
