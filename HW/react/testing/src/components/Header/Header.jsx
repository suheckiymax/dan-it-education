import React, {useContext} from 'react';
import {NavLink, Link} from "react-router-dom";
import {logo, list, grid} from "../../utils/index";
import CardsModeContext from "../../context/cardsViewContext";
import './Header.scss';

const Header = () => {
    const {setCardsView} = useContext(CardsModeContext);

    return (
        <header className='header'>
            <div className="container header__inner">
                <div className='header__logo'>
                    <Link to='/' className='header__logo-link'>
                        {logo()} <h3 className='header__logo-title'>YourSmart</h3>
                    </Link>
                </div>
                <ul className='nav'>
                    <li className='nav__item'>
                        <NavLink to='/' className='nav__item-link'>Home</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink to='/cart' className='nav__item-link'>Cart</NavLink>
                    </li>
                    <li className='nav__item'>
                        <NavLink to='/favorites' className='nav__item-link'>Favorites</NavLink>
                    </li>
                </ul>
                <div className="header__control">
                    <div className='header__control-item' onClick={() => setCardsView('list')}>
                        {list()}
                    </div>
                    <div className='header__control-item' onClick={() => setCardsView('grid')}>
                        {grid()}
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header