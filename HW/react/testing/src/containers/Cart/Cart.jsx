import React, {useContext} from "react";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import ProductList from "../../components/ProductList/ProductList";
import {useSelector} from "react-redux";
import {selectData} from "../../redux/getData/selectors";
import {useProductHook} from "../../utils/customHooks/useProductHook";
import CardsViewContext from "../../context/cardsViewContext";
import './Cart.scss';

const Cart = () => {
    const listData = useSelector(selectData);
    const {activeProduct, favoriteProduct} = useProductHook();
    const {cardsView} = useContext(CardsViewContext);

    const products = listData.filter(product => product.inCart);
    if (!products.length) return <section className='cart cart__default-text'>No products selected</section>

    return (
        <section className='cart container'>
            <OrderSummary/>
            <ProductList list={products}
                         favoriteProduct={favoriteProduct}
                         handleClick={activeProduct}
                         status='delete'
                         btnClasses='cart__control-delete'
                         btnText='Delete from cart'
                         isCart={true}
                         option={cardsView}
            />
        </section>
    )
}

export default Cart