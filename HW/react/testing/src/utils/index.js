import {checkDataStorage} from './dataStorage'

export {
    favorite,
    logo,
    list,
    grid
} from './icons-svg'

export {
    checkDataStorage
}