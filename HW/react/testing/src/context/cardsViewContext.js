import React, {createContext} from "react";

const CardsModeContext = createContext('grid');

export default CardsModeContext
