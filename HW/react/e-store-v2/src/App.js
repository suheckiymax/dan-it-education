import React, {useEffect, useState} from 'react';
import {fetchData} from "./utils/API";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Home from "./containers/Home/Home"
import Cart from "./containers/Cart/Cart";
import Favorites from "./containers/Favorites/Favorites";
import Page404 from "./containers/NotFound/NotFound";
import {Routes, Route} from 'react-router-dom';
import modalWindowDeclarations from "./utils/modalWindows";
import './assets/styles/main.scss';

const App = () => {
  const [products, setProducts] = useState([])
  const [product, setProduct] = useState('')
  const [status, setStatus] = useState('')
  const [modal, setModal] = useState('')

  useEffect(() => {
    fetchData(setProducts)
  }, [])

  const closeModal = () => {
    setStatus('');
    setModal('');
  }

  const activeProduct = (modalID, product) => {
    const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);

    setStatus(modalID)
    setModal(modalDeclaration)
    setProduct(product)
  }

  const favoriteProduct = code => {
    let favoriteProducts = [];

    if (products.find(product => product.code === code)) {
      favoriteProducts = products.map(product =>
          product.code === code ? {...product, isFavorite: !product.isFavorite} : product
      )

      setProducts(favoriteProducts)
    }

    const favoriteProduct = favoriteProducts.filter(product => product.isFavorite ? product.code : '')

    if (favoriteProduct.length === 0) {
      localStorage.removeItem('favorites');
    } else {
      localStorage.setItem('favorites', JSON.stringify(favoriteProduct))
    }
  }

  const addToCart = productCart => {
    let cartProducts = [];

    if (products.find(product => product.code === productCart.code)) {
      cartProducts = products.map(product =>
          product.code === productCart.code ? {...product, inCart: true} : product
      )

      setProducts(cartProducts)
    }

    const productsInCart = cartProducts.filter(product => product.inCart)
    localStorage.setItem('cart', JSON.stringify(productsInCart));

    closeModal();
  }

  return (
      <>
        <Header/>
        <main className="main">
          <Routes>
            <Route path='/' element={<Home list={products}
                                           status={status}
                                           modal={modal}
                                           product={product}
                                           closeModal={closeModal}
                                           favoriteProduct={favoriteProduct}
                                           addToCart={addToCart}
                                           activeProduct={activeProduct}/>}/>
            <Route path='/cart' element={<Cart list={products}
                                               status={status}
                                               modal={modal}
                                               product={product}
                                               setProducts={setProducts}
                                               closeModal={closeModal}
                                               activeProduct={activeProduct}/>} />

            <Route path='/favorites' element={<Favorites list={products}
                                                         status={status}
                                                         modal={modal}
                                                         product={product}
                                                         closeModal={closeModal}
                                                         addToCart={addToCart}
                                                         favoriteProduct={favoriteProduct}
                                                         activeProduct={activeProduct}/>}/>
            <Route element={<Page404/>}/>
          </Routes>
        </main>
        <Footer/>
      </>
  )
}

export default App