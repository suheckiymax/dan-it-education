import PropTypes from 'prop-types'
import './Button.scss'

const Button = ({classes, handleClick, text}) => {
    return <button className={classes} onClick={handleClick}>{text}</button>
}

export default Button

Button.propTypes = {
    classes: PropTypes.string,
    handleClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
}

Button.defaultProps = {
    classes: 'default-Button',
}