import { Link } from "react-router-dom";
import './Header.scss';

const Header = () => {
    return (
        <header className='header'>
            <div className='header__nav'>
                <nav className='nav'>
                    <Link to='/' className='nav__item-link'>Home</Link>
                    <Link to='/cart' className='nav__item-link'>Cart</Link>
                    <Link to='/favorites' className='nav__item-link'>Favorites</Link>
                </nav>
            </div>
        </header>
    )
}

export default Header
