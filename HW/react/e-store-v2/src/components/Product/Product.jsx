import React from 'react';
import PropTypes from 'prop-types';
import { favorite } from "../../utils/icons-svg";
import Button from "../Button/Button";
import './Product.scss';

const Product = ({product, picture, name, color, price, handleClick, favoriteProduct, isFavorite, code} ) => {
    return (
        <div className='product'>
            <div className='product__header'>
                <p className='product__header-code'>Code: {code}</p>
                <button className='product__header-btn' onClick={() => favoriteProduct(code)}>
                    {isFavorite ? favorite('1', 'product__header-favorite') : favorite('0', 'product__header-favorite')}
                </button>
            </div>
            <div className='product__about'>
                <a href="#">
                    <img src={picture} alt={name} className='product__header-img'/>
                </a>
                <p className='product__about-title'>{name} <br/> ({color})</p>
            </div>
            <div className='product__control'>
                <p className='product__control-price'>{price}</p>
                <Button classes="product__control-btn"
                        text='Add to cart'
                        handleClick={() => handleClick('add', product)}/>
            </div>
        </div>
    )
}

export default Product

Product.propTypes = {
    picture: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired,
    favoriteProduct: PropTypes.func.isRequired,
    code: PropTypes.string.isRequired
}

Product.defaultProps = {
    color: ''
}