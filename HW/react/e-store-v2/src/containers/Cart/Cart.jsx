import React from "react";
import Button from "../../components/Button/Button";
import './Cart.scss'
import Modal from "../../components/Modal/Modal";

const Cart = ({list, status, modal, product, setProducts, closeModal, activeProduct}) => {
    const deleteFromCart = delProduct => {
        const updatedProducts = list.map(product => {
            if (product.code === delProduct.code) return {...product, inCart: false}
            return product
        })

        const cartStorage = JSON.parse(localStorage.getItem('cart'));
        const updateCartStorage = cartStorage.filter(product => product.code !== delProduct.code)

        if (cartStorage.length === 1) {
            localStorage.removeItem('cart');
        } else {
            localStorage.setItem('cart', JSON.stringify(updateCartStorage));
        }

        setProducts(updatedProducts)

        closeModal();
    }

    const products = list.filter(product => product.inCart)
    if (!products.length) return <section className='cart cart__default-text'>No products selected</section>

    const productsCart = products.map(product =>
        <div className='cart__product' key={product.code}>
            <div className='cart__product-picture-box'>
                <a href="#">
                    <img src={product.picture} alt={product.name} className='cart__product-picture'/>
                </a>
            </div>
            <div className='cart__product-info'>
                <div className='cart__product-desc'>
                    <p>Code: {product.code}</p>
                    <p>Product: {product.name}</p>
                    <p>Color: {product.color}</p>
                    <p>Price: {product.price}</p>
                </div>
                <div className='cart__product-control'>
                    <Button classes='cart__product-control-confirm' text='Buy now'
                            handleClick={() => activeProduct('buy', product)}/>
                    <Button classes='cart__product-control-delete' text='Delete from cart'
                            handleClick={() => activeProduct('delete', product)}/>
                </div>
            </div>
        </div>
    )

    return (
        <section className='cart'>
            <div className="cart__wrapper">
                {productsCart}
            </div>
            {status && <Modal id={modal.id}
                              title={modal.title}
                              desc={modal.desc}
                              closeModal={() => closeModal()}
                              closeButton={modal.closeButton}
                              actions={{
                                  submit: <Button classes='modal__btn modal__btn--confirm' text={modal.confirm}
                                              handleClick={() => deleteFromCart(product)}/>,
                                  closeBtn: <Button classes='modal__btn modal__btn--cancel' text={modal.cancel}
                                                  handleClick={() => closeModal()}/>
                              }}/>}
        </section>
    )
}

export default Cart