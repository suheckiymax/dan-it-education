import React from "react";
import './Favorites.scss'
import Product from "../../components/Product/Product";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

const Favorites = ({list, status, modal, product, closeModal, addToCart, favoriteProduct, activeProduct}) => {
    const products = list.filter(product => product.isFavorite)
    if (!products.length) return <section className='favorite favorite__default-text'>No featured products</section>

    const productsList = products.map(product =>
        <Product key={product.code}
                 code={product.code}
                 name={product.name}
                 price={product.price}
                 picture={product.picture}
                 color={product.color}
                 inCart={product.inCart}
                 handleClick={activeProduct}
                 isFavorite={product.isFavorite}
                 product={product}
                 favoriteProduct={favoriteProduct}/>
    )

    return (
        <section className='favorite'>
            {productsList}
            {status && <Modal id={modal.id}
                              title={modal.title}
                              desc={modal.desc}
                              closeModal={() => closeModal()}
                              closeButton={modal.closeButton}
                              actions={{
                                  submit: <Button classes='modal__btn modal__btn--confirm' text={modal.confirm}
                                              handleClick={() => addToCart(product)}/>,
                                  closeBtn: <Button classes='modal__btn modal__btn--cancel' text={modal.cancel}
                                                  handleClick={() => closeModal()}/>
                              }}/>}
        </section>
    )
}

export default Favorites