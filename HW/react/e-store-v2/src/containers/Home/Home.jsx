import React from "react";
import ProductList from "../../components/ProductList/ProductList";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import './Home.scss';

const Home = ({list, status, modal, product, closeModal, favoriteProduct, addToCart, activeProduct}) => {
    return (
        <section className='products'>
            <ProductList list={list}
                         favoriteProduct={favoriteProduct}
                         handleClick={activeProduct}/>
            {status && <Modal id={modal.id}
                              title={modal.title}
                              desc={modal.desc}
                              closeModal={() => closeModal()}
                              closeButton={modal.closeButton}
                              actions={{
                                  submit: <Button classes='modal__btn modal__btn--confirm' text={modal.confirm}
                                              handleClick={() => addToCart(product)}/>,
                                  closeBtn: <Button classes='modal__btn modal__btn--cancel' text={modal.cancel}
                                                  handleClick={() => closeModal()}/>
                              }}/>}
        </section>
    )
}

export default Home