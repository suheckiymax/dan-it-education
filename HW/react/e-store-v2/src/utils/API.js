export const fetchData = (setProducts) => {
    fetch('products.json')
        .then(res => res.json())
        .then(data => {
            const dataProducts = data.map(product => ({...product, isFavorite: false, inCart: false}));
            const favoritesStorage = JSON.parse(localStorage.getItem('favorites'));
            const cartStorage = JSON.parse(localStorage.getItem('cart'));

            let updatedData = dataProducts;

            if (cartStorage) {
                updatedData = dataProducts.map(product => {
                    const cartProduct = cartStorage.find(cartProduct => product.code === cartProduct.code);
                    return {...product, inCart: cartProduct ? true : product.inCart}
                })
            }

            if (favoritesStorage) {
                updatedData = updatedData.map(product => {
                    const favoriteProduct = favoritesStorage.find(favProduct => product.code === favProduct.code);
                    return {...product, isFavorite: favoriteProduct ? true : product.isFavorite}
                })
            }

            setProducts(updatedData)
        })
}