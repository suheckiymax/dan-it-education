const modalWindowDeclarations = [
    {
        id: 'firstModal',
        title: 'Do you want to delete this file?',
        desc: 'Once you delete this file, it wont be possible to undo this action. Are you sure you want to delete it?',
        closeButton: true,
        confirm: 'Delete',
        cancel: 'Cancel'
    },
    {
        id: 'secondModal',
        title: 'Do you want to add this file?',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        closeButton: true,
        confirm: 'Add',
        cancel: 'Exit'
    }
]

export default modalWindowDeclarations