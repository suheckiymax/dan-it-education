import React, {Component} from "react";
import './Button.scss';

class Button extends Component {
    render() {
        const {backgroundColor, text, handleClick, id} = this.props;

        return <button id={id} onClick={handleClick} className={`button ${backgroundColor}`}>{text}</button>
    }
}

export default Button