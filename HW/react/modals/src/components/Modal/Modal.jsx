import React, {Component} from "react";
import './Modal.scss';

class Modal extends Component {
    render() {
        const {title, text, closeModal, closeButton, actions: {submit, closeBtn}} = this.props;

        return (
            <div className='modal' onClick={e => (e.currentTarget === e.target) && closeModal()}>
                <div className='modal__container'>
                    <div className='modal__header'>
                        <span className='modal__header-title'>{title}</span>
                        {closeButton && <span className="modal__header-close" onClick={closeModal}/>}
                    </div>
                    <div className="modal__content">
                        <p className='modal__content-text'>{text}</p>
                    </div>
                    <div className='modal__control'>
                        {submit}
                        {closeBtn}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal