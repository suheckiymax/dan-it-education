import React, {Component} from "react";
import modalWindowDeclarations from "./utils/modalWindowDeclarations";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import './App.scss';

class App extends Component {
    state = {
        status: '',
        modal: {}
    }

    closeModal = () => {
        this.setState({
            status: '',
            modal: {}
        })
    }

    showModal() {
        const {id, title, desc, closeButton, confirm, cancel} = this.state.modal;

        return (
            <Modal
                id={id}
                title={title}
                text={desc}
                closeModal={() => this.closeModal()}
                closeButton={closeButton}
                actions={{
                    submit: <button className='modal__control-btn'
                                    onClick={() => this.closeModal()}>{confirm}</button>,
                    closeBtn: <button className='modal__control-btn'
                                      onClick={() => this.closeModal()}>{cancel}</button>
                }}
            />
        )
    }

    getModal(e) {
        const modalID = e.target.id;
        const modalDeclaration = modalWindowDeclarations.find(item => item.id === modalID);

        this.setState({
            status: modalID,
            modal: modalDeclaration
        })
    }

    render() {
        return (
            <>
                <div className="content">
                    <div className="content__control">
                        <Button id="firstModal"
                                backgroundColor="button--primary"
                                text="Open first modal"
                                handleClick={e => this.getModal(e)}
                        />
                        <Button id="secondModal"
                                backgroundColor="button--secondary"
                                text="Open second modal"
                                handleClick={e => this.getModal(e)}
                        />
                    </div>
                </div>
                {this.state.status && this.showModal()}
            </>
        )
    }
}

export default App;
