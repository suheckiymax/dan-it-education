import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from 'redux-thunk'
import getDataReducer from "./getData";
import activeModalReducer from "./modalReducer";
import activeProductReducer from "./productReducer";
import storageMiddleware from "./middlewars/storageMiddleware";

const store = createStore(
    combineReducers({
        data: getDataReducer,
        modal: activeModalReducer,
        product: activeProductReducer
    }),
    applyMiddleware(thunk, storageMiddleware)
)

export default store