import React, {useEffect} from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRouter from "./router/AppRouter";
import {useDispatch} from "react-redux";
import {getDataThunk} from "./redux/getData/actions";
import './assets/styles/main.scss';

const App = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getDataThunk())
    }, [dispatch])

    return (
        <>
            <Header/>
            <main className='main'>
                <AppRouter />
            </main>
            <Footer/>
        </>
    )
}

export default App