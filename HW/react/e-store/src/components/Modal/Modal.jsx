import React, {Component} from "react";
import PropTypes from 'prop-types';
import './Modal.scss';

export default class Modal extends Component {
    render() {
        const {title, desc, closeModal, closeButton, actions: {submit, closeBtn}} = this.props;

        return (
            <div className='modal' onClick={e => (e.currentTarget === e.target) && closeModal()}>
                <div className='modal__container'>
                    <div className='modal__header'>
                        <span className='modal__header-title'>{title}</span>
                        {closeButton && <span className="modal__header-close" onClick={closeModal}/>}
                    </div>
                    <div className="modal__content">
                        <p className='modal__content-text'>{desc}</p>
                    </div>
                    <div className='modal__control'>
                        {submit}
                        {closeBtn}
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    desc: PropTypes.string.isRequired,
    closeModal: PropTypes.func.isRequired,
    closeButton: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.element).isRequired
}

Modal.defaultProps = {
    title: 'Confirm action',
    closeButton: true
}