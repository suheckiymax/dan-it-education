import React, {Component} from "react";
import {favoriteIcon, cartIcon} from "../../utils";
import './Header.scss';

export default class Header extends Component {
    state = {
        favoriteProductsCount: 0,
        productsCount: 0,
    }

    componentDidMount() {
        const favoritesProducts = JSON.parse(localStorage.getItem('favorites'));
        const cartProducts = JSON.parse(localStorage.getItem('carts'));

        this.setState({
            favoriteProductsCount: favoritesProducts?.length || 0,
            productsCount: cartProducts?.length || 0,
        })
    }

    render() {
        const {productsCount, favoriteProductsCount} = this.props;

        return (
            <header className="header">
                <nav className="nav">
                    <div className="nav__link">
                        <span className="nav__count">
                            {favoriteProductsCount === 0 ? this.state.favoriteProductsCount : favoriteProductsCount}
                        </span>
                        {favoriteIcon('0', 'nav__favorite-icon')}
                    </div>
                    <div className="nav__link">
                        <span className="nav__count">
                            {productsCount === 0 ? this.state.productsCount : productsCount}
                        </span>
                        {cartIcon('nav__cart-icon')}
                    </div>
                </nav>
            </header>
        )
    }
}