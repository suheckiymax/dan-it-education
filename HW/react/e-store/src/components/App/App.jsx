import React, {Component} from "react";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import ProductsList from "../ProductList/ProductsList";
import Header from "../Header/Header";
import {modalsDeclaration} from "../../utils";
import './App.scss';

export default class App extends Component {
    state = {
        products: [],
        activeProduct: '',
        status: '',
        modal: '',
        favoriteProductsCount: 0,
        productsCount: 0,
        loading: true
    }

    componentDidMount() {
        fetch('products.json')
            .then(res => res.json())
            .then(data => {
                const dataProducts = data.map(product => ({...product, isFavorite: false}));
                const favoritesStorage = JSON.parse(localStorage.getItem('favorites'));

                if (favoritesStorage) {
                    const updatedData = dataProducts.map(product => {
                        const favoriteProduct = favoritesStorage.find(favProduct => product.code === favProduct.code);
                        return {...product, isFavorite: favoriteProduct ? true : product.isFavorite}
                    })

                    this.setState({products: updatedData});
                } else {
                    this.setState({products: dataProducts});
                }

                this.setState({loading: false})
            })
    }

    addToCart() {
        const cart = JSON.parse(localStorage.getItem('carts'));

        if (cart) {
            cart.push(this.state.activeProduct);
            localStorage.setItem('carts', JSON.stringify(cart));
        } else {
            localStorage.setItem('carts', JSON.stringify([this.state.activeProduct]));
        }

        this.setState(prev => ({
            ...prev,
            productsCount: prev.productsCount + 1
        }));

        this.closeModal();
    }

    addToFavorite = code => {
        let favoriteProducts = [];

        if (this.state.products.find(product => product.code === code)) {
            favoriteProducts = this.state.products.map(product =>
                product.code === code ? {...product, isFavorite: !product.isFavorite} : product
            )

            this.setState({
                products: favoriteProducts
            })
        }

        const favoriteProductsCode = favoriteProducts.filter(product => product.isFavorite ? product.code : '');
        localStorage.setItem('favorites', JSON.stringify(favoriteProductsCode));

        this.setState(prev => ({
            ...prev,
            favoriteProductsCount: prev.favoriteProductsCount + 1
        }));
    }

    closeModal = () => {
        this.setState({
            status: '',
            modal: ''
        })
    }

    showModal() {
        return <Modal id={this.state.modal.id}
                      title={this.state.modal.title}
                      desc={this.state.modal.desc}
                      closeModal={() => this.closeModal()}
                      closeButton={this.state.modal.closeButton}
                      actions={{
                          submit: <Button classes='modal__btn modal__btn--confirm' text={this.state.modal.confirm}
                                      handleClick={() => this.addToCart()}/>,
                          closeBtn: <Button classes='modal__btn modal__btn--cancel' text={this.state.modal.cancel}
                                          handleClick={() => this.closeModal()}/>
                      }}/>
    }

    activeProduct = (modalID, product) => {
        const modalDeclaration = modalsDeclaration.find(item => item.id === modalID);

        this.setState({
            status: modalID,
            modal: modalDeclaration,
            activeProduct: product
        })
    }

    render() {
        if (!this.state.products) return [];

        return (
            <>
                <Header productsCount={this.state.productsCount} favoriteProductsCount={this.state.favoriteProductsCount}/>
                {this.state.loading === true ? <span className="loader-text">Loading...</span> :
                    <ProductsList list={this.state.products}
                                  addToFavorite={this.addToFavorite}
                                  handleClick={this.activeProduct}
                    />
                }
                {this.state.status && this.showModal()}
            </>
        )
    }
}
