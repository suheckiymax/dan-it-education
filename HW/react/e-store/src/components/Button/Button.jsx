import React, {Component} from "react";
import PropTypes from "prop-types";
import './Button.scss';

export default class Button extends Component {
    render() {
        const {classes, handleClick, text} = this.props;
        return <button className={classes} onClick={handleClick}>{text}</button>
    }
}

Button.propTypes = {
    classes: PropTypes.string,
    handleClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
}

Button.defaultProps = {
    classes: 'button'
}