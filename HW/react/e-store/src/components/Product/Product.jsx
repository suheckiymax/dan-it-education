import React, {Component} from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import {favoriteIcon} from "../../utils";
import './Product.scss';

export default class Product extends Component {
    render() {
        const {
            product,
            picture,
            name,
            color,
            price,
            handleClick,
            addToFavorite,
            isFavorite,
            code
        } = this.props;

        return (
            <div className='product'>
                <div className='product__header'>
                    <p className='product__header-code'>Code: {code}</p>
                    <button className='product__header-btn' onClick={() => addToFavorite(code)}>
                        {isFavorite ?
                            favoriteIcon('1', 'product__header-favorite') :
                            favoriteIcon('0', 'product__header-favorite')
                        }
                    </button>
                </div>
                <div className='product__about'>
                    <img src={picture} alt={name} className='product__header-img'/>
                    <p className='product__about-title'>{name} <br/> ({color})</p>
                </div>
                <div className='product__control'>
                    <p className='product__control-price'>{price}</p>
                    <Button classes="button button--primary"
                            text='Add to cart'
                            handleClick={() => handleClick('add', product)}/>
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    picture: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    price: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    code: PropTypes.string.isRequired
}

Product.defaultProps = {
    color: ''
}