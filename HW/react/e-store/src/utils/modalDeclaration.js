export const modalsDeclaration = [
    {
        id: 'delete',
        title: 'Do you want to remove this product?',
        desc: 'Are you sure you want to remove this item from your cart?',
        closeButton: true,
        confirm: 'Delete',
        cancel: 'Cancel'
    },
    {
        id: 'add',
        title: 'Do you want to add this product?',
        desc: 'Are you sure you want to add this product to your cart?',
        closeButton: true,
        confirm: 'Add',
        cancel: 'Exit'
    }
]